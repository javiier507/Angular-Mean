import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

//  MODULOS PERSONALIZADOS

import { AuthModule } from './modules/auth/auth.module';

//  COMPONENTES

import { AppComponent } from './app.component';
import { AppNavbarComponent } from './components/app-navbar/app-navbar.component';
import { HomeComponent } from './components/home/home.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';

import { TasksComponent } from './components/tasks/tasks.component';
import { TaskShowComponent } from './components/task-show/task-show.component';
import { TaskEditComponent } from './components/task-edit/task-edit.component';
import { TaskCreateComponent } from './components/task-edit/task-create.component';

//  SERVICIOS

import { TaskService } from './services/task.service';

//  RUTAS

const routes: Routes = [
    { path: '', component: HomeComponent },
    {
        path: 'tasks', 
        //component: TasksComponent,
        children : [
            { path: '', component: TasksComponent },
            { path: 'task/:id', component: TaskShowComponent },
            { path: 'edit/:id', component: TaskEditComponent },
            { path: 'create', component: TaskCreateComponent }   
        ]
    },
    { path: '**', component: PageNotFoundComponent }
];

@NgModule({
    declarations: [
        AppComponent,
        AppNavbarComponent,
        HomeComponent,
        PageNotFoundComponent,
        TasksComponent,
        TaskShowComponent,
        TaskEditComponent,
        TaskCreateComponent
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot(routes),
        FormsModule,
        HttpClientModule,
        NgbModule.forRoot(),
        AuthModule
    ],
    providers: [
        TaskService
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }
