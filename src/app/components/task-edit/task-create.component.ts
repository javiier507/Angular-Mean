import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

import { Task } from '../../services/task';
import { TaskService } from '../../services/task.service';

@Component({
    selector: 'app-task-edit',
    templateUrl: './task-edit.component.html',
    styleUrls: ['./task-edit.component.css']
})
export class TaskCreateComponent implements OnInit {

    task = new Task('', '', '');

    edit = false;

    constructor(
        private router: Router,
        private taskService: TaskService
    ) { }

    ngOnInit() {
        
        

    }

    get tracer() {
        return JSON.stringify(this.task);
    }

    addTask(task: Task): void {
        this.taskService.addTask(task).subscribe((data) => {
            console.log(data);
            this.router.navigate(['/tasks']);

        }, (error) => {
            console.log(error);
        });
    }

    clean(): void {
        this.task =  new Task('', '', '');
    }
}
