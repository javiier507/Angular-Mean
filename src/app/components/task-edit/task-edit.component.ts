import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";

import { Task } from '../../services/task';
import { TaskService } from '../../services/task.service';

@Component({
    selector: 'app-task-edit',
    templateUrl: './task-edit.component.html',
    styleUrls: ['./task-edit.component.css']
})
export class TaskEditComponent implements OnInit {

    task = new Task('', '', '');

    edit = true;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private taskService: TaskService
    ) { }

    ngOnInit() {
        
        this.getTakById();

    }

    getTakById(): void {
        this.route.paramMap.subscribe((params)=> {
            
            const id = params.get('id');

            this.taskService.getTakById(id).subscribe((data) => {
                this.task = data;
            });

        });
    }

    get tracer() {
        return JSON.stringify(this.task);
    }

    updateTask(task: Task): void {
        this.taskService.updateTask(task).subscribe((data) => {
            console.log(data);
        });
    }

    deleteTask(id: string): void {
        this.taskService.deleteTask(id).subscribe((data) => {
            console.log(data);
            this.router.navigate(['/tasks']);
            
        }, (error) => {
            console.log(error);
        });
    }

}
