import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";

import { Task } from '../../services/task';
import { TaskService } from '../../services/task.service';
import { parse } from 'querystring';

@Component({
    selector: 'app-task-show',
    templateUrl: './task-show.component.html',
    styleUrls: ['./task-show.component.css']
})
export class TaskShowComponent implements OnInit {

    task: Task;

    constructor(
        private route: ActivatedRoute, 
        private taskService: TaskService
    ) {}

    ngOnInit() {
        this.getTakById();
    }

    getTakById(): void {
        this.route.paramMap.subscribe((params)=> {
            
            const id = params.get('id');
            console.log(`route param observable : ${id}`);

            this.taskService.getTakById(id).subscribe((data) => {
                console.log(data);
                this.task = data;
            });

        });
    }
}
