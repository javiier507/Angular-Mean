import { Component, OnInit, DoCheck } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../modules/auth/services/auth.service';

@Component({
    selector: 'app-navbar',
    templateUrl: './app-navbar.component.html',
    styleUrls: ['./app-navbar.component.css']
})
export class AppNavbarComponent implements OnInit, DoCheck {

    login: boolean = false;

    constructor(
        private authService: AuthService,
        private router: Router
    ) { }

    ngOnInit() {
        
    }

    ngDoCheck() {
        this.login = this.authService.isAuthenticated();
    }

    goLogin(): void {
        this.router.navigate(['/login']);
    }

    logout(): void {
        this.authService.logout();
    }

}
