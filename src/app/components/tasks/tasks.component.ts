import { Component, OnInit } from '@angular/core';

import { Task } from '../../services/task';
import { TaskService } from '../../services/task.service';

@Component({
    selector: 'app-tasks',
    templateUrl: './tasks.component.html',
    styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {

    tasks: Task[];

    constructor(private taskService: TaskService) { }

    ngOnInit() {

        this.getTaks();
    }

    getTaks(): void {
        this.taskService.getTaks().subscribe((data) => {
            console.log(data);
            this.tasks = data;
        }, (error) => {
            console.log(error);
        });
    }

}
