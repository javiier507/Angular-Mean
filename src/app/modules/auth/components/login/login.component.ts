import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../services/auth.service';
import { User } from '../../services/user';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    user = new User('', '', '', '', '');

    constructor(
        private authService: AuthService,
        private router: Router
    ) { }

    ngOnInit() {

        //  MOVE GUARD
        if(this.authService.isAuthenticated())
            this.router.navigate(['/']);
    }

    login(user: User): void {
        this.authService.login(user).subscribe((data) => {
            console.log(data);
            this.authService.setSession(user, data['token']);
            this.router.navigate(['/profile']);

        }, (error) => {
            console.log(error);
        });
    }

}
