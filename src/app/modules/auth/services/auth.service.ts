import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import {tokenNotExpired} from 'angular2-jwt';


import { User } from './user';

const httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class AuthService {

    private apiUrl = 'http://localhost:3000/auth';

    constructor(private http: HttpClient) { }

    login(user: User)  {
        return this.http.post(this.apiUrl+'/login', user, httpOptions);
    }

    logout(): void {
        localStorage.clear();
    }

    profile() {
        return this.http.get(this.apiUrl+'/profile');
    }

    //  HELPERS

    setSession(user: User, token: string): void {
        localStorage.setItem('user', JSON.stringify(user));
        localStorage.setItem('token', token);
    }

    getUser(): string {
        return localStorage.getItem('user');
    }

    getToken(): string {
        return localStorage.getItem('token');
    }

    isAuthenticated(): boolean {
        return tokenNotExpired();
    }
}
